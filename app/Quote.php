<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['text', 'author_id'];

    /**
     * Get the author that said the quote.
     */
    public function author()
    {
        return $this->belongsTo('App\Author');
    }
}
