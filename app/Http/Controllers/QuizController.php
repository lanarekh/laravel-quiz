<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewQuizRequest;
use App\Http\Requests\NextQuizRequest;
use App\Services\QuizService;

class QuizController extends Controller
{
    /**
     * Start a new Quiz Session.
     *
     * @param  \App\Http\Requests\NewQuizRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function new(NewQuizRequest $request)
    {
        $quizService = new QuizService($request->all());
        $response = $quizService->generateResponse();
        session($quizService->returnSessionArray());
        return response()->json($response)->header('Access-Control-Allow-Credentials', 'true');
    }

    /**
     * Continue a Quiz Session.
     *
     * @param  \App\Http\Requests\NextQuizRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function next(NextQuizRequest $request)
    {
        $quizService = new QuizService(session()->all());
        $response = $quizService->generateResponse($request->input('answer'));
        session($quizService->returnSessionArray());
        return response()->json($response)->header('Access-Control-Allow-Credentials', 'true');
    }
}
