<?php

namespace App\Services;

use App\Quote;
use App\Author;

class QuizService
{
    public $mode;
    public $questionNo;
    public $quoteId;
    public $result;

    public function __construct($sessionArray)
    {
        $this->mode = $sessionArray['mode'] ?? 'binary';
        $this->answer = $sessionArray['answer'] ?? '';
        $this->questionNo = (int) ($sessionArray['questionNo'] ?? '0');
        $this->quoteId = (int) ($sessionArray['quoteId'] ?? '0');
        $this->result = (int) ($sessionArray['result'] ?? '0');
    }

    private function getQuote()
    {
        return Quote::cursor()->random(1)->first();
    }

    private function getAnswers(Quote $quote, $mode)
    {
        $realAuthorName = $quote->author->name;
        $authorsArray = Author::cursor()->random(3)->map(function ($author) {
            return $author->name;
        })->all();
        if (!in_array($realAuthorName, $authorsArray)) {
            $authorsArray[0] = $realAuthorName;
            shuffle($authorsArray);
        }

        if ($mode === 'binary') {
            return $authorsArray[0];
        } else {
            return $authorsArray;
        }
    }

    private function compare($answer)
    {
        $authorName = Quote::find($this->quoteId)->author->name;
        if ($this->mode === 'binary') {
            $correct = (($authorName === $this->answer) == $answer);
        } else {
            $correct = ($authorName === $answer);
        }
        $this->answer = $authorName;
        if ($correct) {
            $this->result++;
        }
        $this->questionNo++;
    }

    private function newQuestion()
    {
        $previousAnswer = $this->answer;
        $quote = $this->getQuote();
        $this->quoteId = $quote->id;
        $answers = $this->getAnswers($quote, $this->mode);
        ($this->mode === 'binary') ? $this->answer = $answers : null;

        return [
            'quote' => $quote->text,
            'questionNo' => $this->questionNo,
            'result' => $this->result,
            'prevAnswer' => $previousAnswer,
            'mode' => $this->mode,
            'answers' => $answers
        ];
    }

    public function returnSessionArray()
    {
        return [
            'mode' => $this->mode,
            'answer' => $this->answer,
            'questionNo' => (string) $this->questionNo,
            'quoteId' => (string) $this->quoteId,
            'result' => (string) $this->result,
        ];
    }

    public function generateResponse($answer=null)
    {
        if ($this->questionNo === 0) {
            $this->questionNo++;
            return $this->newQuestion();
        } elseif ($this->questionNo < 10) {
            $this->compare($answer);
            return $this->newQuestion();
        } else {
            $this->compare($answer);
            $response = [
                'mode' => $this->mode,
                'prevAnswer' => $this->answer,
                'questionNo' => (string) --$this->questionNo,
                'quoteId' => (string) $this->quoteId,
                'result' => (string) $this->result,
            ];
            $this->questionNo = 0;
            $this->quoteId = 0;
            $this->result = 0;
            return $response;
        }
    }
}
