<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $ilia = App\Author::create(['name' => 'Ilia Chavchavadze']);
        App\Quote::create([
            'text' => 'Is man a human?',
            'author_id' => $ilia->id
        ]);
        App\Quote::create([
            'text' => 'Man creates their destiny, and destiny does not create the man.',
            'author_id' => $ilia->id
        ]);
        App\Quote::create([
            'text' => 'Sometimes short word is also long.',
            'author_id' => $ilia->id
        ]);
        $shota = App\Author::create(['name' => 'Shota Rustaveli']);
        App\Quote::create([
            'text' => 'What you give to others is yours, what you do not is lost.',
            'author_id' => $shota->id
        ]);
        App\Quote::create([
            'text' => "Lion's cub is still a lion's cub, wheter it's a boy or a girl.",
            'author_id' => $shota->id
        ]);
        App\Quote::create([
            'text' => "Long word can be said in short, that's what poetry is good for.",
            'author_id' => $shota->id
        ]);
        $luka = App\Author::create(['name' => 'Luka Tvaliashvili']);
        App\Quote::create([
            'text' => "It's important what's in your head, not on your shelf.",
            'author_id' => $luka->id
        ]);
        $orbeliani = App\Author::create(['name' => 'Sulkhan-Saba Orbeliani']);
        App\Quote::create([
            'text' => 'Man should be valued with their work and execution.',
            'author_id' => $orbeliani->id
        ]);
        $unknown = App\Author::create(['name' => 'Unknown']);
        App\Quote::create([
            'text' => "Good kid is the rose of their mom.",
            'author_id' => $unknown->id
        ]);
        App\Quote::create([
            'text' => 'Whatever happens to you David, everything with your head.',
            'author_id' => $unknown->id
        ]);
        App\Quote::create([
            'text' => 'If you tie a bull near a bull, it will change its nature or color.',
            'author_id' => $unknown->id
        ]);
        $iakobi = App\Author::create(['name' => 'Iakob Gogebashvili']);
        App\Quote::create([
            'text' => 'As poor people are, more they need school. Which is considered fairly as the best medicine for the poor.',
            'author_id' => $iakobi->id
        ]);
        $akaki = App\Author::create(['name' => 'Akaki Tsereteli']);
        App\Quote::create([
            'text' => 'Even if you kill the swallow, spring will still come.',
            'author_id' => $akaki->id
        ]);
    }
}
