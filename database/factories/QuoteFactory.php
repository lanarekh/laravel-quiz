<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Quote;
use App\Author;
use Faker\Generator as Faker;

$factory->define(Quote::class, function (Faker $faker) {
    return [
        'text' => $faker->text($maxNbChars = 320),
        'author_id' => function () {
            return factory(Author::class)->create()->id;
        },
    ];
});
